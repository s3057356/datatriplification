import sys
import pandas as pd
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, RDFS, XSD


def main(csv_path):
    # Read the CSV file
    print("Reading the CSV file...")
    csvdata = pd.read_csv(csv_path)
    print(f"Data from '{csv_path}' loaded successfully.")\
    
    # Create a graph and define namespaces
    g = Graph()
    SAREF = Namespace("https://saref.etsi.org/core/")
    EX = Namespace("http://example.org/")
    g.bind("saref", SAREF)
    g.bind("ex", EX)

    # Triplification
    print("Starting triplification...")
    for col_name in csvdata.columns:
        if col_name in ["utc_timestamp", "cet_cest_timestamp", "interpolated"]:
            continue

        device_uri = URIRef(EX[col_name])
        g.add((device_uri, RDF.type, SAREF.Device))
        g.add((device_uri, SAREF.hasFunction, URIRef(EX.MeteringFunction)))
        measurement_counter = 0

        for i, row in csvdata.iterrows():
            if pd.isna(row[col_name]):
                continue
            measurement_uri = URIRef(EX[f"{col_name}_{measurement_counter}"])
            g.add((measurement_uri, RDF.type, SAREF.Measurement))
            g.add((measurement_uri, SAREF.hasTimestamp, Literal(row['utc_timestamp'], datatype=XSD.dateTime)))
            g.add((measurement_uri, SAREF.hasValue, Literal(row[col_name], datatype=XSD.decimal)))
            g.add((measurement_uri, SAREF.isMeasuredIn, Literal("kWh")))
            g.add((measurement_uri, SAREF.relatesToProperty, SAREF.Energy))
            g.add((device_uri, SAREF.makesMeasurement, measurement_uri))
            measurement_counter += 1

    # Save graph as Turtle file
    print("Serializing graph to Turtle format...")
    g.serialize(destination='graph.ttl', format='turtle')
    print("Process complete! Graph saved as 'graph.ttl'")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Please provide the path to the csv as the 2nd argument")
        quit()
    else:
        csv_path = sys.argv[1]
        main(csv_path)
